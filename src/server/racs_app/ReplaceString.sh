#Copyright (c) 2014 Nokia Solutions and Networks. All rights reserved.
#  Application: GoOn
#
#  Component: Startup scripts
#
#  File: ReplaceString.sh
#
#  Purpose:
#Helper script


line="base-image.qcow2"
rep=$1.qcow2
sed -i "s/$line/$rep/g" ./$1.xml

