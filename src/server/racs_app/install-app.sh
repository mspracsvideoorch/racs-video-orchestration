#!/bin/sh
# Licensed Materials - Property of IBM
# <Product-id>
# � Copyright IBM Corp. 2012  All Rights Reserved
#
# Installs the sample application onto a base image

set -e

usage() {
    echo "Usage:"
    echo "  ${0##*/} --in srcimage --out destimage --src srcdir --name applicationName"
    echo "     srcimage is the path to the base qcow2 image"
    echo "     destimage is the name of the installed disk image"
    echo "     srcdir is the directory containing files to copy or append"
	echo "     name is the application Name"
    exit 1
}

mount_image()
{
    IMG=$1
    MNT=$2
    echo "Mounting $IMG on $MNT"
    echo "  creating mountpoint"
    mkdir -p $MNT
    echo "  guestmounting $IMG on $MNT"
    #guestmount -o allow_other -o kernel_cache -a $IMG -m /dev/vda1 $MNT
	guestmount -o allow_other -o kernel_cache -a $IMG -i $MNT
    # guestmount fails with zero exit code so sanity check
    if ! test -f $MNT/etc/inittab ; then
        echo "ERROR: guestmount probably failed" 1>&2
        umount $MNT || true
        exit 1
    fi
}

umount_image()
{
    IMG=$1
    MNT=$2

    kvmpid=`ps -C kvm -C qemu-system-x86_64 -www -o pid=,args= | \
              fgrep file=$IMG | awk '{ print $1 }'`
    if [ -z "$kvmpid" ]; then
        echo "ERROR: Failed to find kvm process" 1>&2
        exit 1
    fi

    echo "Unmounting $IMG from $MNT"
    umount $MNT

    count=120
    while [ $count -gt 0 ]; do
        if [ -d "/proc/$kvmpid" ]; then
            echo "  waiting for guestmount kvm process to exit"
            sleep 1
            count=$((count-1))
        else
            break
        fi
    done
    if [ $count -le 0 ]; then
        echo "ERROR: Timeout waiting for kvm process to exit" 1>&2
    fi

    echo "  removing mountpoint"
    rmdir $MNT
}

if [ $# -lt 6 ]; then
    usage
fi

while [ $# -ne 0 ]; do
    case $1 in
        --in)
            SRCIMAGE=$2
            shift
            ;;
        --out)
            DESTIMAGE=$2
            shift
            ;;
        --src)
            SRC=$2
            shift
            ;;
		--name)
			APP_NAME=$2
			shift
			;;
        --help)
            usage
            ;;
        *)
            echo "Unknown option: $1"
            usage
            ;;
    esac
    shift
done

echo "Creating copy of base image: $SRCIMAGE => $DESTIMAGE"
cp -f $SRCIMAGE $DESTIMAGE

MNT=`mktemp --directory`
trap "rmdir $MNT" EXIT
mount_image "$DESTIMAGE" "$MNT"
trap "umount_image '$DESTIMAGE' '$MNT'" EXIT

################################################################################
# Install files for sample application
################################################################################
echo "Installing files for application"

########### Copy application specific files on the Application Image

INSTALL_BASE=opt/$APP_NAME
INSTALL_DIR=$MNT/$INSTALL_BASE
mkdir -p $INSTALL_DIR
chown root:root $INSTALL_DIR
#script for sending heartbeat messages from application
cp $SRC/heartbeat.py $INSTALL_DIR
#script to mount persistent volumes
cp $SRC/mount-volumes.py $INSTALL_DIR
#scripts to extract certificates from pkcs12 files
cp $SRC/certs.py $INSTALL_DIR
cp $SRC/handle_keystore.sh $INSTALL_DIR
cp $SRC/handle_truststore.sh $INSTALL_DIR
#script to start the application
cp $SRC/start.sh $INSTALL_DIR
#Application class files
cp $SRC/GoOnApp.jar $INSTALL_DIR
#libpcap files
cp $SRC/jnetpcap.jar $INSTALL_DIR
cp $SRC/json-org-0.0.1.jar $INSTALL_DIR
cp $SRC/json-simple-1.1.1.jar $INSTALL_DIR
cp $SRC/libjnetpcap.so $INSTALL_DIR

chmod a+x $INSTALL_DIR/*.sh
chmod a+x $INSTALL_DIR/*.py


#############Replace /etc/rc.d/init.d/network script on the application image with network.py script. network.py script is needed for setting up the network connectivity for the VM

NETWORK_FILE=$MNT/etc/rc.d/init.d/network
cp $SRC/network.py $NETWORK_FILE
chmod a+x $NETWORK_FILE
chown root:root $NETWORK_FILE

############ Copy the MQTT clients and libs to te Application Image

INSTALL_MOSQ=$MNT/$INSTALL_BASE/mosq
INSTALL_LIB=$MNT/usr/lib64
mkdir -p $INSTALL_MOSQ
chown root:root $INSTALL_MOSQ
cp ./mosq/MQTT* $INSTALL_MOSQ
cp ./mosq/mosquitto_sub $INSTALL_MOSQ
cp ./mosq/mosquitto_pub $INSTALL_MOSQ
cp ./mosq/libmosquitto.so.1 $INSTALL_LIB
chmod a+x $INSTALL_MOSQ/*

############Create directory to store the extracted certificates from pkcs12 files provided in ovf-environment

INSTALL_CERTS=$INSTALL_MOSQ/certs
mkdir -p $INSTALL_CERTS
chown root:root $INSTALL_CERTS


#############Copy the Mibs and scripts relevant for OAM purposes to the Application Image

INSTALL_OAM_FM=$INSTALL_DIR/oam/fm
INSTALL_OAM_PM=$INSTALL_DIR/oam/pm
INSTALL_MIB=$MNT/usr/share/snmp/mibs
mkdir -p $INSTALL_OAM_FM
mkdir -p $INSTALL_OAM_PM
chown root:root $INSTALL_OAM_FM
chown root:root $INSTALL_OAM_PM
cp $SRC/oam/fm/sendAlarm.sh $INSTALL_OAM_FM
cp $SRC/oam/fm/Mibs/* $INSTALL_MIB
chmod a+x $INSTALL_OAM_FM/*

##############Copy the NTP scripts to the application Image

INSTALL_NTP=$INSTALL_DIR/ntp
mkdir -p $INSTALL_NTP
chown root:root $INSTALL_NTP
cp $SRC/ntp/* $INSTALL_NTP
chmod a+x $INSTALL_NTP/*


# Mount OVF environment ISO
mkdir -p $MNT/mnt/ovf-environment
cp $SRC/ovf-env.xml $MNT/mnt/ovf-environment #Added by Janne. Copy ready-made environment file to VM.

echo "LABEL=OVF /mnt/ovf-environment iso9660 ro 0 0" >> $MNT/etc/fstab

PACKAGENAME="com.nsn."
PROCESSNAME=$PACKAGENAME$APP_NAME
# Run scripts at startup
echo "/opt/$APP_NAME/start.sh $APP_NAME $PROCESSNAME" >> $MNT/etc/rc.local

################################################################################

trap - EXIT
umount_image "$DESTIMAGE" "$MNT"
echo "Test application image unmounted"
