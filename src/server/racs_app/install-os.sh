# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#  Component: Install scripts
#
#  File: install-os.sh
#
#  Purpose:
# Creates a disk image file and installs Red Hat Enterprise Linux / Cent OS onto it

set -e

usage() {
    echo "Usage:"
    echo "  ${0##*/} --in isofile --out destimage --name applicationName"
    echo "     isofile is the path to the ISO"
    echo "     destimage is the name of the installed disk image, which will be overwritten if it exists"
	echo "     name is the application Name"
    exit 1
}

if [ $# -lt 4 ]; then
    usage
fi

while [ $# -ne 0 ]; do
    case $1 in
        --in)
            ISO_FILE=$2
            shift
            ;;
        --out)
            DISK_IMAGE=$2
            shift
            ;;
		--name)
			APP=$2
			shift
			;;
        --help)
            usage
            ;;
        *)
            echo "Unknown option: $1"
            usage
            ;;
    esac
    shift
done

if [ ! -e $ISO_FILE ]; then
    echo "Input ISO file does not exist: $ISO_FILE" >&2
    exit 2
fi

if [ -e $DISK_IMAGE ]; then
    echo "Deleting existing disk image: $DISK_IMAGE"
    rm -f $DISK_IMAGE
fi

KS_FILE=linux-kickstart.cfg


qemu-img create -f qcow2 -o compat=0.10 $DISK_IMAGE 5G

# Run the install.
# Note that the CPU and RAM values here are only used for the installation.
virt-install \
--name=$APP \
--ram=1024 \
--vcpus=1 \
--arch=x86_64 \
--os-type=linux \
--os-variant=rhel6 \
--hvm \
--virt-type kvm \
--noreboot \
--nonetworks \
--disk path=$DISK_IMAGE,format=qcow2,bus=virtio \
--initrd-inject `dirname $0`/$KS_FILE \
--location=$ISO_FILE \
--graphics none \
--extra-args "console=ttyS0,115200 text headless ks=file:/$KS_FILE"

virsh dumpxml $APP > $APP.xml
virsh destroy $APP || true
virsh undefine $APP || true
