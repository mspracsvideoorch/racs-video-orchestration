#Copyright (c) 2014 Nokia Solutions and Networks. All rights reserved.
#  Application: GoOn
#
#  Component: Instruction script
#
#  File: Readme.txt
#
#  Purpose:
# To Provide instruction to application developer on usage of scripts



This RACS developer package contains scripts required for building ,packaging and configuring applications on LA 20.This package also contains the required files for a sample application 'GoOn'

1. Extract the RACSAppDeveloper.zip file to tmp directory (tmp directory is mandatory as the qemu user will not have permissions to any other directories).

2. Place the application binaries in the application folder (eg.GoOnApp.jar inside GoOn folder),application ovf in subfolder '/GoOn/ovf' and authorization  file in subfolder '/auth'. #Petteri: You can skip this

3. Download MQTT clients mosquitto_sub,mosquitto_pub and libmosquitto.so.1 and place it in '/mosq' directory. Add the below lines to install-app.sh
cp ./mosq/mosquitto_sub $INSTALL_MOSQ
cp ./mosq/mosquitto_pub $INSTALL_MOSQ
cp ./mosq/libmosquitto.so.1 $INSTALL_LIB

#Petteri: I think these were under /usr/bin/ and /usr/lib/

MQTT_sub.py and MQTT_pub.py scripts available in /mosq directory of the package.They could be used to read the MQTT Broker IP  and port from ovf-env.xml and invoke mosquitto_sub/mosquitto_pub for subscribing/publishing a particular topic  
python MQTT_sub.py <mqtt topic name to subscribe>
python MQTT_pub.py <mqtt topic name to publish> <message payload to send>


4. install-app script currently builds the process name like below 
"com.nsn.GoOn" which includes the package name 'com.nsn' also. This process name is built for heartbeat purposes. In non nsn cases, this script will have to be modified accordingly with the correct package name of the respective application.

#Petteri: check the scripts from create_RACS_VM.txtcreate_RACS_VM.txt-file
5. Run the script build-app.sh with ISO file path and application name as input. This command will generate the application image with linux installed on it. It will also generate the OVA file
	For instance, 
	If OS is CentOS-6.4, then run the script with following arguments.
	./build-app.sh --in /home/images/CentOS-6.4-x86_64-bin-DVD1.iso --name GoOn
	
	Following is the command used by the script to install CentOS on the image
	virt-install 
	--name=GoOn \
	--ram=1024 \
	--vcpus=1 \
	--arch=x86_64 \
	--os-type=linux \
	--os-variant=rhel6 \
	--hvm \
	--virt-type kvm \
	--noreboot \
	--nonetworks \
	--disk path= base.qcow2,format=qcow2,bus=virtio \
	--initrd-inject `dirname $0`/$KS_FILE \
	--location= /home/images/CentOS-6.4-x86_64-bin-DVD1.iso\
	--graphics none \
	--extra-args "console=ttyS0,115200 text headless ks=file:/$KS_FILE"
	
	If OS is RHEL 6,then run the script with following arguments.
	./build-app.sh --in /home/images/rhel-server-6.3-x86_64-dvd.iso --name GoOn
	
	Following is the command used by the script to install RHEL on the image
	virt-install 
	--name=GoOn \
	--ram=1024 \
	--vcpus=1 \
	--arch=x86_64 \
	--os-type=linux \
	--os-variant=rhel6 \
	--hvm \
	--virt-type kvm \
	--noreboot \
	--nonetworks \
	--disk path= base.qcow2,format=qcow2,bus=virtio \
	--initrd-inject `dirname $0`/$KS_FILE \
	--location= /home/images/rhel-server-6.3-x86_64-dvd.iso\
	--graphics none \
	--extra-args "console=ttyS0,115200 text headless ks=file:/$KS_FILE"
	
	$KS_FILE = linux-kickstart.cfg is the response file used during OS installation on the image. This file contains the list of packages to be installed from the iso to the application image.It also contains the login details to the application VM.
	
GoOn application has been successfully tested with CentOS-6.4-x86_64-bin-DVD1.iso and rhel-server-6.3-x86_64-dvd.iso

6. Delete the existing base-image.qcow2, GoOn.qcow2 and GoOn.ova before running the build-app.sh script

Steps for Local Testing on a pure Linux server
-------------------------------------------------
The build-app script generates the application ova and application.xml (eg. GoOn.xml). You can test if your application VM boots up successfully on Linux by following the below steps

1. Run the StartVM.sh script with application name as input.
The application VM console will be launched with the above command.
Login: root/passw0rd

HINTS:
-------
Command to 
1. Define the virtual machine .
virsh define <App_Name>.xml

where App_Name is the name of your application. eg. GoOn

2. Start the virtual machine running your application image.
virsh start <App_Name>

3. Start the virtual machine console.
virsh console <App_Name>

4. Stop the virtual machine running your application image.
virsh shutdown <App_Name>

5. Undefine the virtual machine.
virsh undefine <App_Name>

Steps for Testing  on RACS
------------------------------------
1. Copy GoOn.ova to (/home/www/images) on image server integrated to your RACS 
2. Execute the following command on RACS
	cimidep GoOn
	cimi machines
	-->get the GoOn  machine id
	cimistartapp <GoOn machine id>
3. SSH to GoOn application VM can be done from a backend server with the following command
ssh root@<IP_Application IP of RACS> -p 29001

Login: root/passw0rd
