#!/bin/bash
#
# Licensed Materials - Property of IBM
# 5725-L10
# (c) Copyright IBM Corp. 2012, 2013 All Rights Reserved
# US Government Users Restricted Rights - Use, duplication or
# disclosure restricted by GSA ADP Schedule Contract with
# IBM Corp.
#
#
# Generates the appropriate OVF manifest and certificate files and
# packages the sample virtual appliance OVA 

set -e

usage() {
    echo "Usage:"
    echo "  ${0##*/} --ovf file --image file --privatekey privatekey --certificate certificate"
    echo "     ovf is the filename of the OVF descriptor file.  Must be in the same directory as the IMAGE file."
    echo "     image is the filename of the disk image in QCOW2 format.  Must be in the same directory as the OVF file."
    echo "     privatekey is the path to a PEM format unencrypted private used to sign the sample application OVA package"
    echo "     certificate is a the path to a PEM format X.509 certificate matching the specified private key used to verify the OVA signing at deployment"
    exit 1
}

# Format of call is "create_manifest_file manifestfilename fileformanifest1 fileformanifest2 fileformanifest3 ..."
create_manifest_file()
{
    MANIFESTFILE=$1

    if [ -e $MANIFESTFILE ]; then
        rm -f $MANIFESTFILE
    fi
    
    echo "Writing OVF manifest file $MANIFESTFILE"
    
    shift    
    while [ $# -ne 0 ]; do
        FILE=$1
        
        echo "Generating manifest digest for file $FILE"
        
        # OpenSSL prints a line format of "SHA1(filename)= digest" which matches the OVF specification
        DIGEST=`openssl dgst -sha256 -hex $FILE`

        # Write digest to the manifest file in the correct OVF format
        echo "$DIGEST" >> $MANIFESTFILE
        shift
    done
}

# Format of call is "create_cert_file certfilename manifestfilename privatekey certificate"
create_cert_file()
{
    CERTFILE=$1
    MANIFESTFILE=$2
    PRIVATEKEYPEM=$3
    CERTIFICATEPEM=$4
    
    echo "Writing OVF certificate file $CERTFILE"
    
    DIGEST=`openssl dgst -sha256 -hex -sign $PRIVATEKEYPEM $MANIFESTFILE`
    
    # OpenSSL prints a line format of "RSA-SHA256(filename)= signeddigest"
    # Need to remove RSA- prefix to match OVF specification of "SHA1(filename)= signeddigest"
    echo "$DIGEST" | sed -e 's/^RSA-//' > $CERTFILE
    cat $CERTIFICATEPEM >> $CERTFILE
}

if [ $# -ne 8 ]; then
    usage
fi

while [ $# -ne 0 ]; do
    case $1 in
        --ovf)
            OVF=`readlink -e $2`
            shift
            ;;
        --image)
            IMAGE=`readlink -e $2`
            shift
            ;;
        --privatekey)
            PRIVATEKEY=`readlink -e $2`
            shift
            ;;
        --certificate)
            CERTIFICATE=`readlink -e $2`
            shift
            ;;
        --help)
            usage
            ;;
        *)             
            echo "Unknown option: $1"
            usage
            ;;
    esac
    shift
done

TEMP=`echo $OVF | awk -F"/" '{print $NF}'`
BUILD_DIR=`dirname $IMAGE`
PREFIX=${TEMP%%.ovf}
OVANAME=$PREFIX.ova
OVFNAME=$PREFIX.ovf
MANIFESTNAME=$PREFIX.mf
CERTNAME=$PREFIX.cert
IMAGENAME=$PREFIX.qcow2
ACL=auth/acl.xml

pushd $BUILD_DIR

create_manifest_file $MANIFESTNAME $OVFNAME $IMAGENAME $ACL
create_cert_file $CERTNAME $MANIFESTNAME $PRIVATEKEY $CERTIFICATE

echo "Creating OVF TAR file $OVANAME"
tar --format=ustar -cvf $OVANAME $OVFNAME $MANIFESTNAME $CERTNAME $IMAGENAME $ACL
popd
