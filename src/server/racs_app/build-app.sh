#!/bin/sh
# Licensed Materials - Property of IBM
# <Product-id>
# � Copyright IBM Corp. 2012  All Rights Reserved
#
# Builds the application

set -e

usage() {
    echo "Usage:"
    echo "  ${0##*/} --in isofile --name applicationName "
    echo "     isofile is the path to the CentOS /RHEL ISO"
    echo "     name is the application Name"
    exit 1

}

if [ $# -ne 4 ]; then
    usage
    exit 1
fi

while [ $# -ne 0 ]; do
    case $1 in
        --in)
            ISO_FILE=$2
            shift
            ;;
		--name)
			APP_NAME=$2
			shift
			;;
        --help)
            usage
            ;;
        *)
            echo "Unknown option: $1"
            usage
            ;;
    esac
    shift
done

BASE_IMAGE=base-image.qcow2
APP_IMAGE=$APP_NAME.qcow2
SRC_DIR=./$APP_NAME

if [ ! -e $BASE_IMAGE ]; then
    ./install-os.sh --in $ISO_FILE --out $BASE_IMAGE --name $APP_NAME
fi
./install-app.sh --in $BASE_IMAGE --out $APP_IMAGE --src $SRC_DIR --name $APP_NAME

cp $SRC_DIR/ovf/$APP_NAME.ovf .
tar --format=ustar -cvf $APP_NAME.ova $APP_NAME.ovf $APP_IMAGE ./auth/acl.xml
./ReplaceString.sh $APP_NAME
rm -f ./$APP_NAME.ovf
rm -f ./acl.xml
