#!/usr/bin/env python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#  Component: Startup scripts
#
#  File: heartbeat.py
#
#  Purpose:
#
# Sample code to demonstrate sending a heartbeat pulse message.
#
# This code MUST NOT be used in production, when sending heartbeats
# must only be done if the other parts of your application 
# are running correctly.

import socket
import libxml2
import struct
import binascii
import time
import logging
import subprocess
import re
import sys

# Get the heartbeat address,port,intercept ID and application ID from the
# OVF environment file
doc = libxml2.parseFile("/mnt/ovf-environment/ovf-env.xml");
ctxt = doc.xpathNewContext()
ctxt.xpathRegisterNs("aspn", "http://www.ibm.com/schema/aspn/ovf-environment/1");
heartbeat = ctxt.xpathEval("//aspn:Heartbeat")[0]
address = heartbeat.prop("monitorAddress")
(host,port) = address.split(":")
port = int(port)
app_id = long(heartbeat.prop("appId"))
intercepts = ctxt.xpathEval("//aspn:Heartbeat/aspn:Intercept")

# The number of milliseconds before the application should be
# treated as failed if there are no more heartbeat messages.
max_interval_millis = 5000

# Create the heartbeat message.
# Six signed four-byte integers, then one signed eight-byte integer 
msg = struct.pack(">iiiiqii", 0x8945, 0x1212, 1, 0, app_id, max_interval_millis, len(intercepts))

for intercept in intercepts:
    interceptid = intercept.prop("id")
    msg = msg + struct.pack(">q", long(interceptid))

ctxt.xpathFreeContext()
doc.freeDoc()

print "Created heartbeat message:", binascii.hexlify(msg)

#print struct.unpack(">iiiiqiiqq", msg )
appName = sys.argv[1]
def findProcess( processName ):
	ps= subprocess.Popen("ps -ef | grep "+processName , shell=True, stdout=subprocess.PIPE)
	output = ps.stdout.read()
	ps.stdout.close()
	ps.wait()
	return output
	
def isProcessRunning( processName ):
	output = findProcess( processName )
	if re.search(processName , output):
		return True
	else:
		return False
		
socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
socket.connect((host, port))

while True:
	if isProcessRunning (appName):
		socket.send(msg)
	else:
		print "Process Not Running. Not sending heartbeat message"		
	time.sleep(1)
