#!/usr/bin/python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#
#

import sys
import libxml2
import subprocess
import re
import datetime
import os

from netaddr import IPNetwork, IPAddress, valid_ipv4

network_address = ""
netmask = ""
ovf_env_file = "/mnt/ovf-environment/ovf-env.xml"

def validate_ntp_server_ip_addr(ntp_server_ip_addr_list):

    print "inside validate"
    print ntp_server_ip_addr_list
    if validate_ntp_server_ip_addr_regex(ntp_server_ip_addr_list) != 0:
         print "regex validation fail"
         return 1
    if validate_ntp_server_ip_addr_subnet(ntp_server_ip_addr_list) != 0:
         print "subnet validation fail"
         return 1
    return 0

def validate_ntp_server_ip_addr_regex(ntp_server_ip_addr_list):

     num_ntpserver = len(ntp_server_ip_addr_list)
     for entry in xrange(0,num_ntpserver):
          validation_status = validate_ip_addr_regex(ntp_server_ip_addr_list[entry])
          if validation_status != 0:
               return 1
     return 0

def validate_ip_addr_regex(ip_addr):
     addr = ip_addr.split(".")
     if len(addr) != 4:
          print "IPv4 address invalid"
          return 1

     if not valid_ipv4(ip_addr):
          print "IPv4 address invalid"
          return 1

     return 0

def validate_ntp_server_ip_addr_subnet(ntp_server_ip_addr_list):

     num_ntpserver = len(ntp_server_ip_addr_list)
     print num_ntpserver
     get_ntp_server_network_addr_netmask()
     print "sudhish"
     print network_address
     print netmask
     #if IPAddress("192.168.0.10") in IPNetwork("192.168.0.0/255.255.255.0"):
     network_address_netmask = network_address + "/" + netmask
     print network_address_netmask
     for entry in xrange(0,num_ntpserver):
          if IPAddress(ntp_server_ip_addr_list[entry]) not in IPNetwork(network_address_netmask):
               print "ntp server ip - subnet validation failed"
               return 1
     return 0

def get_ntp_server_network_addr_netmask():

     global network_address
     global netmask

     xmlfile = libxml2.parseFile(ovf_env_file);

     ctxt = xmlfile.xpathNewContext()
     ctxt.xpathRegisterNs("ovf", "http://schemas.dmtf.org/ovf/environment/1");
     ctxt.xpathRegisterNs("aspn", "http://www.ibm.com/schema/aspn/ovf-environment/1");

     NetworkInterfaceList = ctxt.xpathEval("//aspn:NetworkInterface")

     for NetworkInterfaceEntry in NetworkInterfaceList:
          networkName = NetworkInterfaceEntry.prop("networkName")
          if "com.nsn.racs.nw.appdefault" == networkName:
               print "network name found"
               ctxt.setContextNode(NetworkInterfaceEntry)
               IPv4Entry = ctxt.xpathEval("./aspn:IPv4")
               for IP in IPv4Entry:
                    print "ipv4 entry"
                    ctxt.setContextNode(IP)
                    RouteEntry = ctxt.xpathEval("./aspn:Route")
                    for Route in RouteEntry:
                         print "route entry"
                         network_address = Route.prop("dest")
                         netmask = Route.prop("mask")
                         print "hi"
                         print network_address
                         print netmask
               break
