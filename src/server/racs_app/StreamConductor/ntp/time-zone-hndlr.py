#!/usr/bin/python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#
#

# This script reads the aspn:TimeZone parameter from ovf_env.xml and sets the
# local timezone accordingly. If aspn:timezone is not available in ovf_env.xml
# , time zone is not changed and remains in UTC and thus compatible with LA10
# behaviour.
# To set the time zone, a symbolic link is created at /etc/localtime that points
# to a file at /usr/share/zoneinfo/ depending on the timezone configured.
# /etc/localtime.original is created as a backup of the /etc/localtime that exists
# the very first time that the script is being run.
 
import sys
import libxml2
import subprocess
import re
import datetime
import os

ovf_env_file = "/mnt/ovf-environment/ovf-env.xml"

def getTimeZone():

     doc = libxml2.parseFile(ovf_env_file);

     ctxt = doc.xpathNewContext()
     ctxt.xpathRegisterNs("ovf", "http://schemas.dmtf.org/ovf/environment/1");
     ctxt.xpathRegisterNs("aspn", "http://www.ibm.com/schema/aspn/ovf-environment/1");

     # parse ovf-env.xml for TimeZone
     TimeZoneInfo = ctxt.xpathEval("//aspn:TimeZone")
     tz = None
     for i in TimeZoneInfo:
          tz = i.prop("ZoneID")
     return tz

def setLocalTimeZone(tz):

     localTimePath="/etc/localtime"
     tzPath = os.path.join( "/usr/share/zoneinfo", tz )
     if os.path.exists( tzPath ):
          if os.path.lexists( localTimePath ):
               if not (os.path.exists(localTimePath + ".original")):
                    os.rename( localTimePath, localTimePath + ".original" )
               os.remove(localTimePath)
               os.symlink( tzPath, localTimePath )
          else:
               os.symlink( tzPath, localTimePath )
     else:
          print "Timezone info found in ovf-env.xml does not seem to be valid (" + tzPath + " apparently does not exist)"
          print " Not changing timezone configuration"


tz = getTimeZone()
if tz:
     setLocalTimeZone(tz)
else:
     print "No timezone info found in ovf-env.xml, not changing timezone configuration"
