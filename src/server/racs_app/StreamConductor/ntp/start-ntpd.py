#!/usr/bin/python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#
#

import sys
import libxml2
import subprocess
import re
import datetime
import os

def run(command):
    #print_with_timestamp( "  running: " + ' '.join(command) )
    if debug:
        return
    return subprocess.call(command)

ntpd_start_script = "/etc/init.d/ntpd"
debug = 0

print "starting ntpd"
run( [ ntpd_start_script, "start" ] )
