#!/usr/bin/python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#
# 

import sys
import libxml2
import subprocess
import re
import datetime
import os


def run(command):
    return subprocess.call(command)

ntpd_conf_mgr_script = "ntpd-conf-mgr.py" #/mnt/ntp-scripts/ removed because that is not where the scripts are.
print ntpd_conf_mgr_script
ntp_port_snat_script = "apply-ntp-port-snat.py" #/mnt/ntp-scripts/ removed because that is not where the scripts are.
print ntp_port_snat_script
ntpd_start_script = "start-ntpd.py" #/mnt/ntp-scripts/ removed because that is not where the scripts are.
print ntpd_start_script
run(ntpd_conf_mgr_script)
run(ntp_port_snat_script)
run(ntpd_start_script)
sys.exit(0)
