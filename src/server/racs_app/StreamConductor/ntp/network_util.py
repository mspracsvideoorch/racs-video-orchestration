#!/usr/bin/python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#
#

import sys
import libxml2
import subprocess
import re
import datetime
import os
import fileinput
import shutil

ovf_env_file = "/mnt/ovf-environment/ovf-env.xml"

def get_app_default_network_interface_l3_addr():

     app_default_network_interface_l3_addr = ""
     xmlfile = libxml2.parseFile(ovf_env_file);

     ctxt = xmlfile.xpathNewContext()
     ctxt.xpathRegisterNs("ovf", "http://schemas.dmtf.org/ovf/environment/1");
     ctxt.xpathRegisterNs("aspn", "http://www.ibm.com/schema/aspn/ovf-environment/1");

     NetworkInterfaceList = ctxt.xpathEval("//aspn:NetworkInterface")

     for NetworkInterfaceEntry in NetworkInterfaceList:
          networkName = NetworkInterfaceEntry.prop("networkName")
          if "com.nsn.racs.nw.appdefault" == networkName:
               print "network name found"
               ctxt.setContextNode(NetworkInterfaceEntry)
               IPv4Entry = ctxt.xpathEval("./aspn:IPv4")
               for IP in IPv4Entry:
                    print "ipv4 entry"
                    app_default_network_interface_l3_addr = IP.prop("address")
                    print app_default_network_interface_l3_addr
                    return app_default_network_interface_l3_addr
     return app_default_network_interface_l3_addr

def get_network_interface_name_from_l3_addr(ipv4_address):

     interface_name = ""
     ip_addr_show = subprocess.Popen(['ip', 'addr', 'show'], stdout=subprocess.PIPE);
     for line in ip_addr_show.stdout:
          if re.search(ipv4_address,line):
               print line
               ipv4entry = line.split(" ")
               print ipv4entry
               print len(ipv4entry)
               interface_name = ipv4entry[len(ipv4entry) - 1]
               interface_name = interface_name.split("\n")
               print "interface_name"
               interface_name = interface_name[0]
               print interface_name
               return interface_name
     return interface_name

def get_app_default_network_interface_name():

     app_default_network_interface_name = ""
     app_default_network_interface_l3_addr = get_app_default_network_interface_l3_addr()
     print "inside interface name"
     print app_default_network_interface_l3_addr
     app_default_network_interface_name = get_network_interface_name_from_l3_addr(app_default_network_interface_l3_addr)
     return app_default_network_interface_name

