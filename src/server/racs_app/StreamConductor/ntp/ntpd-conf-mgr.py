#!/usr/bin/python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#
#

import sys
import libxml2
import subprocess
import re
import datetime
import os
import fileinput
import shutil

from validate_ntp_server_ip import validate_ntp_server_ip_addr
from network_util import get_app_default_network_interface_name

ovf_env_file = "/mnt/ovf-environment/ovf-env.xml"

def get_ntp_server_ip_from_ovf():

    xmlfile = libxml2.parseFile(ovf_env_file);

    ctxt = xmlfile.xpathNewContext()
    ctxt.xpathRegisterNs("ovf", "http://schemas.dmtf.org/ovf/environment/1");
    ctxt.xpathRegisterNs("aspn", "http://www.ibm.com/schema/aspn/ovf-environment/1");

    print "successfully parsed the ovf-xml"

    enwi = ctxt.xpathEval( "//ovf:Property" )

    for ei in enwi:
        info_e = ei.prop( "key" )
        if "ntp.servers" in info_e.lower():
            ntp_servers = ei.prop( "value" )
            print "ntp server list"
            print ntp_servers
            return ntp_servers

def update_ntp_conf(ip_list_object):

     ntp_conf_file_banner_line1 = "# NTP configuration file used by NTP daemon /usr/sbin/ntpd" + "\n"
     ntp_conf_file_banner_line2 = "# conf details are given in app developer guide" + "\n"
     ntp_conf_file = open("/etc/ntp_vm.conf",'w')
     ntp_conf_file.write(ntp_conf_file_banner_line1 + ntp_conf_file_banner_line2)

     ntp_conf_file.write("\n")
     ntp_conf_file.write("# default access settings" + "\n")
     ntp_conf_file.write("restrict default ignore" + "\n" + "restrict -6 default ignore" + "\n")
     ntp_conf_file.write("\n")
     ntp_conf_file.write("# local host access settings" + "\n")
     ntp_conf_file.write("restrict 127.0.0.1" + "\n" + "restrict -6 ::1" + "\n")
     ntp_conf_file.write("\n")
     ntp_conf_file.write("# ntp server conf and access settings" + "\n")

     num_ntpserver = len(ip_list_object)
     for entry in xrange(0,num_ntpserver):
          ntp_server_restrict_entry = "restrict -4 " + ip_list_object[entry] + " kod nomodify notrap nopeer noquery" + "\n" 
          ntp_server_conf_entry = "server -4 " + ip_list_object[entry] + " iburst" + "\n"
          ntp_conf_file.write(ntp_server_restrict_entry + ntp_server_conf_entry)

     ntp_conf_file.write("\n")
     ntp_conf_file.write("# drift file path" + "\n")
     ntp_conf_file.write("driftfile /var/lib/ntp/drift")
     ntp_conf_file.close()

def update_ntpd_cmd_line_options():

     ntpd_cmd_line_options_filename = "/etc/sysconfig/ntpd"
     if os.path.exists(ntpd_cmd_line_options_filename):
          print "/etc/sysconfig/ntpd exists"
          if not (os.path.exists(ntpd_cmd_line_options_filename + ".original")):
               print "original does not exist"
               shutil.copyfile( ntpd_cmd_line_options_filename, ntpd_cmd_line_options_filename + ".original" )
     else:
          print "/etc/sysconfig/ntpd file does not exist. ntpd cannot be started"
          sys.exit(1)

     current_options=""
     for line in fileinput.input(["/etc/sysconfig/ntpd.original"]):
    # sys.stdout is redirected to the file
          if re.match("^OPTIONS=", line):
               current_options = line 
     new_options = current_options.split("\"")
     app_default_network_interface_name = get_app_default_network_interface_name()
     print "inside cmd options"
     print app_default_network_interface_name
     new_options = new_options[0] + "\"" + new_options[1] + " -c /etc/ntp_vm.conf" + " -I " + app_default_network_interface_name + " -I lo" + "\""
     print new_options
     ntp_cmd_line_options_file = open("/etc/sysconfig/ntpd",'w')
     ntp_cmd_line_options_file.write("# ntpd command line options" + "\n")
     ntp_cmd_line_options_file.write(new_options)
     ntp_cmd_line_options_file.close()


if os.path.isfile(ovf_env_file):
    print ovf_env_file
    ntp_server_list = get_ntp_server_ip_from_ovf()
    print ntp_server_list
    ntp_server_ip_list = ntp_server_list.split(",")
    if validate_ntp_server_ip_addr(ntp_server_ip_list) != 0:
         print "ntp server ip address validation fail"
         sys.exit(1)
    update_ntpd_cmd_line_options()
    update_ntp_conf(ntp_server_ip_list)
else:
    print "ovf-env.xml does not exist....exiting"
    sys.exit(1)

