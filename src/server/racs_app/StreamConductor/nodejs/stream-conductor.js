var WebSocketServer = require('ws').Server;
var fs = require('fs');
var events = require('events');
var express = require('express');
var bodyParser = require('body-parser');
var spawnSync = require('spawn-sync');  // in Node.js 0.12 or later, just do "require('child_process').spawnSync"
var spawn = require('child_process').spawn;
var randomstring = require("randomstring");

var live555Address = "127.0.0.1";
var live555Port = "40003";
var app = express();
var wsServer = new WebSocketServer({port: 8081});
var streamInfoFile = '/tmp/live555_conns'; // TODO: change filepath to wherever the connection file of live555 is
var streamInfo = {};
var streamInfoWatcher = new events.EventEmitter();


var live555StreamCount = 0;
var availableStreamIndices = [];

// Get number for new stream, but don't increment live555StreamCount or pop availableStreamIndices (don't expect that number is succesfully used)
function getNewStreamNumber() {
  if (availableStreamIndices.length === 0) {
    return live555StreamCount;
  }
  return availableStreamIndices[0];
}

function useStreamNumber(number) {
  if (number >= live555StreamCount) {
    live555StreamCount++;
  }
  else {
    var index = availableStreamIndices.indexOf(number);
    if (index < 0) {
      throw new Error("Index error when trying to use a stream number");
    }
    availableStreamIndices.splice(index, 1);
  }
}

function freeStreamNumber(number) {
  availableStreamIndices.push(number);
}


// Start live555ProxyServer
var live555Username = randomstring.generate(6);
var live555Password = randomstring.generate(10);
var live555Process = spawn("live/proxyServer/live555ProxyServer", ["-R", "-U", live555Username, live555Password]);
live555Process.stdout.on('data', function (data) {
  console.log('Live555 stdout: ' + data);
});
live555Process.stderr.on('data', function (data) {
  console.log('Live555 stderr: ' + data);
});
live555Process.on('close', function (code) {
  console.log('Live555 process exited with code ' + code);
  throw new Error('Live555 process died');
});


// #############################################################################
// ###################### INIT SERVER FROM FILE ################################

// If streamInfoFile exists, read it's content to streamInfo
// Reregister any streams found in the file to live555 proxy

try {
  var streamInfoFileContent = fs.readFileSync(streamInfoFile, 'utf-8');
  streamInfo = JSON.parse(streamInfoFileContent);

  for (var streamId in streamInfo) {
    if (streamInfo.hasOwnProperty(streamId)) {
      var streamNumber = parseInt(streamId);
      if (streamNumber >= live555StreamCount) {
        live555StreamCount = streamNumber + 1;
      }
      var registerer = spawnSync("live/testProgs/registerRTSPStream", ["-u", live555Username, live555Password, live555Address, live555Port, streamInfo[streamId]["rtspAddress"], streamId], {timeout: 500});  // timeout milliseconds
      // TODO: check if register fails here
    }
  }

  // Add all indices to availableStreamIndices
  for (var i = 0; i < live555StreamCount; i++) {
    availableStreamIndices.push(i);
  }
  // Remove indices that are in use (registered)
  for (var streamId in streamInfo) {
    if (streamInfo.hasOwnProperty(streamId)) {
      var streamNumber = parseInt(streamId);
      var index = availableStreamIndices.indexOf(streamNumber);
      if (index < 0) {
        throw new Error("Index error when reading streamInfoFile");
      }
      availableStreamIndices.splice(index, 1);
    }
  }
}
catch (e) {
  if (e.code === 'ENOENT') {
    // No streamInfoFile, everything OK, no need to react.
  } 
  else {
    throw e;
  }
}

// #############################################################################
// ######################## WEBSOCKET SERVER ###################################

// Handle WebSocket clients
wsServer.on('connection', function(ws) {
  // if stream info has changed, push updated info to clients
  streamInfoWatcher.on('change', function() {
    ws.send('STREAM_INFO\n' + JSON.stringify(streamInfo));
  });

  // print any messages from clients for debugging purposes
  ws.on('message', function(message) {
    console.log('Received message to WebSocket: %s', message);
  });
  
  // send stream info to new clients when they connect
  ws.send('STREAM_INFO\n' + JSON.stringify(streamInfo));
});


// #############################################################################
// ###################### HTTP SERVER (EXPRESS) ################################

app.use(bodyParser.json());  // for parsing application/json
//app.use(bodyParser.urlencoded({ extended: true }));  // for parsing application/x-www-form-urlencoded

app.post('/add/stream', function(req, res) {
  if (!req.body.hasOwnProperty("rtspAddress")) {
    res.status(400).send("No RTSP address defined");  // 400: Bad Request
    return;
  }
  var rtspAddress = req.body["rtspAddress"];

  // check if stream with the same rtspAddress is already registered
  for (var streamId in streamInfo) {
    if (streamInfo[streamId]["rtspAddress"] === rtspAddress) {
      res.status(400).send("The stream is already registered");  // 400: Bad Request
      return;
    }
  }

  var streamNumber = getNewStreamNumber();
  var streamSuffix = streamNumber.toString();

  var registerer = spawnSync("live/testProgs/registerRTSPStream", ["-u", live555Username, live555Password, live555Address, live555Port, req.body["rtspAddress"], streamSuffix], {timeout: 500});  // timeout milliseconds
  if (registerer.error) {
    console.log("Registerer timeout or non-zero exit code");
    res.status(404).send("Server-side error with registering");
    return;
  }
  // if streaming proxy server is not yet online
  if (registerer.stderr.toString().indexOf("Connection to server failed: Connection refused") > -1) {
    res.status(404).send("Streaming proxy server offline");
    return;
  }


  // Success, add stream
  useStreamNumber(streamNumber);
  streamInfo[streamSuffix] = {"rtspAddress": rtspAddress};
  streamInfoWatcher.emit('change');


  fs.writeFile(streamInfoFile, JSON.stringify(streamInfo), function(err) {
    res.send(JSON.stringify({"streamId": streamSuffix}));
    console.log("New stream added");
  });
  
});

app.post('/delete/stream', function(req, res) {
  if (!req.body.hasOwnProperty("streamId")) {
    res.status(400).send("No stream id defined");  // 400: Bad Request
    return;
  }
  var streamId = req.body["streamId"];
  if (!streamInfo.hasOwnProperty(streamId)) {
    res.status(400).send("No stream with the given stream id exists");  // 400: Bad Request
    return;
  }

  freeStreamNumber(parseInt(streamId));
  delete streamInfo[streamId];
  streamInfoWatcher.emit('change');

  fs.writeFile(streamInfoFile, JSON.stringify(streamInfo), function(err) {
    console.log("Stream deleted");
    res.send("Stream deleted");
  });

});

app.post('/set/metadata', function(req, res) {
  var hasChanged = false;
  // edit this variable and set it's value to streamInfo in the end if everything is ok
  var updatedStreamInfo = streamInfo;
  console.log("Metadata change request:\n" + JSON.stringify(req.body));

  for (var streamId in req.body) {
    if (req.body.hasOwnProperty(streamId)) {
      if (!updatedStreamInfo.hasOwnProperty(streamId)) {
        res.status(400).send("Invalid request. Trying to edit non-existing stream(s)");  // 400: Bad Request
        return;
      }
      for (var streamProperty in req.body[streamId]) {
        if (["author", "title", "description"].indexOf(streamProperty) < 0) {
          continue;
        }
        if ((!updatedStreamInfo[streamId].hasOwnProperty(streamProperty)) || (updatedStreamInfo[streamId][streamProperty] !== req.body[streamId][streamProperty])) {
          updatedStreamInfo[streamId][streamProperty] = req.body[streamId][streamProperty];
          hasChanged = true;
        }

      }
    }
  }

  if (hasChanged) {
    streamInfo = updatedStreamInfo;
    streamInfoWatcher.emit('change');

    fs.writeFile(streamInfoFile, JSON.stringify(streamInfo), function(err) {
      console.log("Metadata modified");
      res.send("Metadata modified");
    });
  }
  else {
    res.send("No need for changes in metadata");
  }

});

app.get('/get/streams', function(req, res) {
  res.send(JSON.stringify(streamInfo));
});


var httpServer = app.listen(8080, function() {
  var host = httpServer.address().address;
  var port = httpServer.address().port;
  console.log('Express app listening at http://%s:%s', host, port);
});



// Tester for command line:
//
// Register stream:
// curl -d '{"rtspAddress":"rtsp://fake.address/stream0"}' -H "Content-Type: application/json" http://127.0.0.1:8080/add/stream