#!/usr/bin/env python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# � Copyright IBM Corp. 2012  All Rights Reserved
#
#  Component: Startup scripts
#
#  File: network.py
#
#  Purpose:
# Sample code to set up networking.
# Can be used as a direct replacement for /etc/rc.d/init.d/network
# on Red Hat Enterprise Linux V6.
#
# Usage: network.py {start,stop} [ovf-env.xml [resolv.conf]]

import sys
import libxml2
import subprocess
import re
import subprocess

def print_usage():
    print "Usage:", sys.argv[0], "{start,stop} [ovf-env.xml [resolv.conf]]"
    sys.exit(1)

if (len(sys.argv) < 2):
    print_usage()
    
command = sys.argv[1]

if command == 'stop':
    sys.exit(0)
elif command != 'start':
    print_usage()

debug = 0
ovfenvfile = "/mnt/ovf-environment/ovf-env.xml"
if (len(sys.argv) > 2):
    ovfenvfile = sys.argv[2]
    debug = 1

print "Starting networks defined in", ovfenvfile

resolvconffile = "/etc/resolv.conf"
if (len(sys.argv) > 3):
    resolvconffile = sys.argv[3]

def run(command):
    print "  running: " + ' '.join(command)
    if debug:
        return
    return subprocess.call(command)

iplinkshow = subprocess.Popen(['ip', 'link', 'show'], stdout=subprocess.PIPE);
name_re = re.compile('^\d+:\s+([^:]+):');
mac_re = re.compile('^\s+link/ether\s+(\S+)');
mac_map = {}
for line in iplinkshow.stdout:
    m = name_re.match(line)
    if m is not None:
        name = m.group(1)
    else:
        m = mac_re.match(line)
        if m is not None:
            mac = m.group(1).lower()
            mac_map[mac] = name
            print "ip link: " + mac + ": " + name

doc = libxml2.parseFile(ovfenvfile);

ctxt = doc.xpathNewContext()
ctxt.xpathRegisterNs("ovf", "http://schemas.dmtf.org/ovf/environment/1");
ctxt.xpathRegisterNs("aspn", "http://www.ibm.com/schema/aspn/ovf-environment/1");
res = ctxt.xpathEval("//aspn:NetworkInterface")
dns_domain = None
dns_search = []
dns_server = []
for item in res:
    name = item.prop("networkName")
    mac = item.prop("mac")
    mac = mac.lower()
    gwMac = item.prop("gwMac")
    gwMac = gwMac.lower()
    print "OVF " + name + ": " + mac
    if mac not in mac_map:
        print "OVF network not found: " + name + "/" + mac 
        if debug:
            mac_map[mac] = name
        else:
            continue

    dev = mac_map[mac]
    print "  name:", dev

    route_commands = []
    ctxt.setContextNode(item)
    ipv4 = ctxt.xpathEval("./aspn:IPv4")
    for ip in ipv4:
        address = ip.prop("address")
        netmask = ip.prop("netmask")
        run(["ip", "addr", "add",  address+"/"+netmask, "dev", dev]);
	#adding arp commands
	#route_commands.append(["arp", "-s", via , gwMac]) 
        ctxt.setContextNode(ip)
        routes = ctxt.xpathEval("./aspn:Route")
        for r in routes:
            dest = r.prop("dest")
            if dest is None:
                break
            mask = r.prop("mask")
            if mask is None:
                break
            via = r.prop("via")
            if via is None:
                break
            # can't do this yet as interface is still down
            route_commands.append(["ip", "route", "add", dest+"/"+mask, "via", via, "dev", dev])
            route_commands.append(["arp", "-s", via , gwMac])
	#subprocess.call(['arp', '-s',  via , gwMac]) 
    mtu = item.prop("mtu")
    if mtu is not None:
        print "  mtu:", mtu
        run(["ip", "link", "set", dev, 'mtu', mtu]);

    print "  bringing up interface"
    run(["ip", "link", "set", dev, "up"]);
    print "  setting alias"
    run(["ip", "link", "set", dev, "alias", name]);

    ctxt.setContextNode(item)
    dns = ctxt.xpathEval("./aspn:DNS")
    if dns:
        domain = dns[0].prop("domain")
        if dns_domain is None:
            dns_domain = domain
        print "  dns domain:", domain
        dns_search.append(domain)
        ctxt.setContextNode(dns[0])
        for server in ctxt.xpathEval("./aspn:Server"):
            address = server.prop("address")
            print "  dns server:", address
            dns_server.append(address)

    for command in route_commands:
        run(command);

subprocess.call(["sysctl -w net.ipv4.ip_forward=1"], shell=True)
#subprocess.call(["brctl addbr mybridge"], shell=True)
#subprocess.call(["brctl addif mybridge `ifconfig | grep 192.168.254 -B 2 | grep eth | awk '{print$1}' | head -1`"], shell=True)
#subprocess.call(["brctl addif mybridge `ifconfig | grep 192.168.254 -B 2 | grep eth | awk '{print$1}' | tail -1`"], shell=True) 
#subprocess.call(["ifconfig `ifconfig | grep 192.168.254 -B 2 | grep eth | awk '{print$1}' | head -1` 0.0.0.0"], shell=True)
#subprocess.call(["ifconfig `ifconfig | grep 192.168.254 -B 2 | grep eth | awk '{print$1}' | tail -1` 0.0.0.0"], shell=True)
#subprocess.call(["ifconfig mybridge up"], shell=True)
if dns_domain is not None:
    # write /etc/resolv.conf
    f = open(resolvconffile, 'w')
    f.write("domain " + dns_domain + "\n")
    f.write("search " + ' '.join(dns_search) + "\n")
    f.write("server " + ' '.join(dns_server) + "\n")
    f.close()
