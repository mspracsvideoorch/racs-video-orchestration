#!/bin/sh
#
#Copyright (c) 2014 Nokia Solutions and Networks. All rights reserved.
#  Application: StreamConductor
#
#  Component: Startup scripts
#
#  File: start.sh
#
#  Purpose:
# Launches the StreamConductor application

nohup python /opt/StreamConductor/mount-volumes.py

#nohup python /opt/StreamConductor/certs.py /opt/StreamConductor /opt/StreamConductor/mosq/certs

nohup python /opt/StreamConductor/ntp/ntpd-controller.py

#Creating pm directory in application persistent volume
mkdir -p $MNT/mnt/StreamConductor.volume1/pm

#Starting the application
rpm -Uvh /opt/$1/nodejs/nodejs-0.10.4-5.1.x86_64.rpm
nohup node /opt/$1/nodejs/stream-conductor.js &

nohup python /opt/$1/heartbeat.py $2 &

#Run the script MQTT_pub.py if your application wants to publish messages via MQTT.
#Commenting this line as mosquitto_pub from MQTT is not packaged with StreamConductor app
#python /opt/$1/mosq/MQTT_pub.py GoOn TestGoOn &
