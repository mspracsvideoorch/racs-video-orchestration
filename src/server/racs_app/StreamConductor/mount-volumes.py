#!/usr/bin/env python
# coding: utf-8
#
# Licensed Materials - Property of IBM
# <Product-id>
# © Copyright IBM Corp. 2012  All Rights Reserved
#
#  Component: Startup scripts
#
#  File: mount-volumes.py
#
#  Purpose:
#
# Sample code to mount persistent volumes, and format
# them using ext4.

import os
import subprocess
import sys
import libxml2

def mount(device, mount_point):
    print "Mounting " + device + " on " + mount_point
    rc = subprocess.call(["mount", "-t", "ext4", f, mount_point])
    print rc
    return rc

doc = libxml2.parseFile("/mnt/ovf-environment/ovf-env.xml");
ctxt = doc.xpathNewContext()
ctxt.xpathRegisterNs("ovf", "http://schemas.dmtf.org/ovf/environment/1");
ctxt.xpathRegisterNs("aspn", "http://www.ibm.com/schema/aspn/ovf-environment/1");

# Find the volumes listed in the OVF environment file
volumes = ctxt.xpathEval("//aspn:Volume")
for volume in volumes:
    disk_id = volume.prop("ovfDiskId")
    pci_address = volume.prop("pciAddress")
    # Search for volume, using the PCI address supplied
    for i in range(0,9):
        f = "/dev/disk/by-path/pci-" + pci_address + "-virtio-pci-virtio" + `i`
        if os.path.exists(f):
            mount_point = "/mnt/" + disk_id
            os.makedirs(mount_point) 
            rc = mount(f, mount_point)
            if rc != 0:
                print "Mount failed.  Creating filesystem on device."
                subprocess.call(["mkfs.ext4", "-L", disk_id, f])
                mount(f, mount_point)
                
