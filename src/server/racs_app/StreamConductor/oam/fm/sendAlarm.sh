#!/bin/sh
#
#Copyright (c) 2014 Nokia Solutions and Networks. All rights reserved.
#  Application: GoOn
#
#  Component: OAM script
#
#  File: sendAlarm.sh
#
#  Purpose:
# Sends alarm to NetAct


set -e

snmptrap \
-v 2c \
-c public $1 \
\"\" \
NOKIA-ENHANCED-SNMP-SOLUTION-SUITE-ALARM-IRP::noiGoOnAlarm \
noiAlarmId i $2 \
noiAlarmEventTime s \"$3\" \
noiAlarmSpecificProblem i $4 \
noiAlarmText s \"$5\" \
noiAlarmPerceivedSeverity i $6 \
noiAlarmAdditionalText s \"$7\" \
noiAlarmProbableCause i $8

