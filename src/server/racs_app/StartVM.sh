#Copyright (c) 2014 Nokia Solutions and Networks. All rights reserved.
#  Application: GoOn
#
#  Component: Startup scripts
#
#  File: StartVM.sh
#
#  Purpose:
# Starts the VM on a linux machine


tar -xvf $1.ova
virsh define $1.xml
virsh start $1
virsh console $1