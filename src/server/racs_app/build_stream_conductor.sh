#!/bin/bash
#Script to build Stream Conductor RACS app.
#If the run does not succeed try "virsh undefine StreamConductor"
ISO_PATH=/home/user/Documents/CentOS-6.5-x86_64-bin-DVD1.iso

rm -f *.qcow2
rm -f *.ova
sudo ./build-app.sh --in $ISO_PATH --name StreamConductor
