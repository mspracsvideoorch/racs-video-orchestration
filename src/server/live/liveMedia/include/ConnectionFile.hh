#ifndef _CONNECTION_FILE_HH
#define _CONNECTION_FILE_HH

#include <UsageEnvironment.hh>
#include <stdio.h>

void addStreamToConnectionFile(UsageEnvironment& env, char const* streamName);

#endif
