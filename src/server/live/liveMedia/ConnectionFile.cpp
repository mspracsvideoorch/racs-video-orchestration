/**********
This library is free software; you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation; either version 2.1 of the License, or (at your
option) any later version. (See <http://www.gnu.org/copyleft/lesser.html>.)

This library is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for
more details.

You should have received a copy of the GNU Lesser General Public License
along with this library; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
**********/
// "liveMedia"
// Copyright (c) 1996-2015 Live Networks, Inc.  All rights reserved.
// Common routines for opening/closing named output files
// Implementation

#if (defined(__WIN32__) || defined(_WIN32)) && !defined(_WIN32_WCE)
#include <io.h>
#include <fcntl.h>
#endif
#ifndef _WIN32_WCE
#include <sys/stat.h>
#endif
#include <string.h>

#include "ConnectionFile.hh"

void addStreamToConnectionFile(UsageEnvironment& env, char const* streamName) {
  FILE* fid;
  char const* filePath = "/tmp/live555_conns";

  fid = fopen(filePath, "a+");
  if (fid == NULL) {
    env.setResultMsg("unable to open file \"", filePath, "\"");
    fclose(fid);
    return;
  }

  const size_t line_size = 300;
  char line[line_size] = ""
  while (fgets(line, line_size, fid) != NULL){
    if (line[strlen(line) - 2] == '\n') //Remove last newline.
      line[strlen(line) - 2] = '\0';
    if (!strcmp(line, streamName)) {
      delete[] line;
      fclose(fid);
      return;
    }
  }

  write(fid, streamName, strlen(streamName));
  write(fid, "\n", 1);

  delete[] line;
  fclose(fid);

  return;
}
/*
int RemoveStreamFromConnectionFile(UsageEnvironment& env, char const* streamName) {
  FILE* fid;
  char const* filePath = "/tmp/live555_conns";

  fid = fopen(filePath, "r+");
  if (fid == NULL) {
    env.setResultMsg("unable to open file \"", filePath, "\"");
    fclose(fid);
    return 1;
  }

  const size_t line_size = 50;
  char line[line_size] = ""
  while (fgets(line, line_size, fid) != NULL){
    if (line[strlen(line) - 2] == '\n') //Remove last newline.
      line[strlen(line) - 2] = '\0';
    if (!strcmp(line, streamName)) {
      delete[] line;
      fclose(fid);
      return 0;
    }
  }

  write(fid, streamName, strlen(streamName));
  write(fid, "\n", 1);

  delete[] line;
  fclose(fid);

  return 0;
}*/
