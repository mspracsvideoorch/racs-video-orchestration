package com.stream.jones.VideoOrchestration;

import android.content.ClipData;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.DragEvent;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.VideoView;

import com.koushikdutta.ion.Ion;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;


public class  MainActivity extends FragmentActivity {

    List<FrameLayout> layouts;
    List<ImageView> previews;
    List<TextView> prevTexts;
    List<ImageView> infoImages;
    List<VideoView> videos;
    List<ImageButton> playbackButtons;
    List<ScrollView> videoTextScrolls;
    List<TextView> videoTexts;
    List<LinearLayout> buttonLayouts;
    List<MediaPlayer> mediaPlayers;

    // The information of server
    public final String serverAddr = "192.168.1.105";
    public final String rtspPort = "40003";
    public final String webSocketPort = "8081";
    public final int mMaxLengthThumbText = 20;

    public int active = -1;
    public int selected = 0;
    public LinearLayout mBackLayout;
    public WebSocketClient mWebSocketClient;
    public JSONObject mStreaminfo;
    public JSONArray mStreamSuffixes;

    public boolean webSocketReady = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setContentView(R.layout.activity_main);

        mBackLayout = (LinearLayout) findViewById(R.id.backLayout);

        layouts = new ArrayList<>();
        prevTexts = new ArrayList<>();
        previews = new ArrayList<>();
        infoImages = new ArrayList<>();
        videos = new ArrayList<>();
        playbackButtons = new ArrayList<>();
        videoTextScrolls = new ArrayList<>();
        videoTexts = new ArrayList<>();
        buttonLayouts = new ArrayList<>();
        mediaPlayers = new ArrayList<>();

        // Add preview layouts to the list
        layouts.add((FrameLayout) findViewById(R.id.previewLayout1));
        layouts.add((FrameLayout) findViewById(R.id.previewLayout2));
        layouts.add((FrameLayout) findViewById(R.id.previewLayout3));

        // Add buttonLayouts to the list
        buttonLayouts.add((LinearLayout) findViewById(R.id.buttonLayout1));
        buttonLayouts.add((LinearLayout) findViewById(R.id.buttonLayout2));
        buttonLayouts.add((LinearLayout) findViewById(R.id.buttonLayout3));

        // Add info images to the list
        infoImages.add((ImageView) findViewById(R.id.infoImage1));
        infoImages.add((ImageView) findViewById(R.id.infoImage2));
        infoImages.add((ImageView) findViewById(R.id.infoImage3));


        // Add videos to the list.
        videos.add((VideoView) findViewById(R.id.videoView1));
        videos.add((VideoView) findViewById(R.id.videoView2));
        videos.add((VideoView) findViewById(R.id.videoView3));

        // Add playback control buttons to the list
        playbackButtons.add((ImageButton) findViewById(R.id.playback_control_button1));
        playbackButtons.add((ImageButton) findViewById(R.id.playback_control_button2));
        playbackButtons.add((ImageButton) findViewById(R.id.playback_control_button3));

        // Add Scroll views in which info text about videos are drawd
        videoTextScrolls.add((ScrollView) findViewById(R.id.videoTextScroll1));
        videoTextScrolls.add((ScrollView) findViewById(R.id.videoTextScroll2));
        videoTextScrolls.add((ScrollView) findViewById(R.id.videoTextScroll3));

        // Add descriptions to the list.
        videoTexts.add((TextView) findViewById(R.id.textView1));
        videoTexts.add((TextView) findViewById(R.id.textView2));
        videoTexts.add((TextView) findViewById(R.id.textView3));

        // Add mediaplayers to the list
        mediaPlayers.add(new MediaPlayer());
        mediaPlayers.add(new MediaPlayer());
        mediaPlayers.add(new MediaPlayer());

        // Add onClickListeners to all buttons
        findViewById(R.id.full_button1).setOnClickListener(clickListener);
        findViewById(R.id.full_button1).setTag(1);
        findViewById(R.id.playback_control_button1).setOnClickListener(clickListener);
        findViewById(R.id.playback_control_button1).setTag(2);
        findViewById(R.id.full_button2).setOnClickListener(clickListener);
        findViewById(R.id.full_button2).setTag(1);
        findViewById(R.id.playback_control_button2).setOnClickListener(clickListener);
        findViewById(R.id.playback_control_button2).setTag(2);
        findViewById(R.id.full_button3).setOnClickListener(clickListener);
        findViewById(R.id.full_button3).setTag(1);
        findViewById(R.id.playback_control_button3).setOnClickListener(clickListener);
        findViewById(R.id.playback_control_button3).setTag(2);

        // Add Touch listener to all videos
        for(int i=0; i<videos.size() ; i++){
            videos.get(i).setId(i);
            videos.get(i).setOnTouchListener(listener);
            videos.get(i).setOnDragListener(dragListener);
        }

        // Wait until websocket thread is ready
        checkConnection();

        // Create scrolling view
        if (webSocketReady)
            createScrollView();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.menu_info:
                Intent intent = new Intent(this, InfoActivity.class);
                startActivity(intent);
                return true;

            case R.id.menu_refresh:
                createScrollView();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onBackPressed() {
        if (getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
            setPreview();
        }

        else {
            moveTaskToBack(true);
            MainActivity.this.finish();
        }

    }

    //############################## LISTENERS ##################################

    // All click events are handled here.
    View.OnClickListener clickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            // Actions for video control buttons. Tag = 1 (fullscreen), tag = 2 (stop button)
            if (v instanceof ImageButton) {
                if ((Integer)v.getTag() == 1) {
                    if(getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
                        setPreview();
                    }
                    else{
                        setFullscreen();
                    }
                } else if ((Integer)v.getTag() == 2) {
                    if (videos.get(active).isPlaying()) {
                        videos.get(active).stopPlayback();
                        ((ImageButton) v).setImageResource(R.drawable.play_button);
                    }
                    else {
                        videos.get(active).resume();
                        infoImages.get(active).setVisibility(View.VISIBLE);
                        ((ImageButton) v).setImageResource(R.drawable.stop_button);
                    }
                }
            }
        }
    };

    // Long clicks are handled here. For drag and drop functionality.
    View.OnLongClickListener longListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick (View v){
            selected = v.getId();
            ClipData data = ClipData.newPlainText("", "");
            View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
            v.startDrag(data, shadowBuilder, v, 0);
            return true;
        }
    };

    // Drag and drop event handler
    View.OnDragListener dragListener = new View.OnDragListener() {
        @Override
        public boolean onDrag(View v, DragEvent ev){
            int action = ev.getAction();

            switch (action) {
                case DragEvent.ACTION_DROP:
                    if(v instanceof VideoView) {
                        startVideo(v.getId());
                        break;
                    }

                default:
                    return true;
            }
            return true;
        }
    };

    // All touch events are handled here.
    View.OnTouchListener listener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View v, MotionEvent event) {

            if (v instanceof VideoView){
                // Hides/shows satusbar and control buttons in fullscreen mode
                if(getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
                    
                    //Show control buttons.
                    if (buttonLayouts.get(active).getVisibility() == View.GONE) {
                        buttonLayouts.get(active).setVisibility(View.VISIBLE);
                    }

                    //Hide the statusbar and control Buttons
                    else{
                        mBackLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
                        buttonLayouts.get(active).setVisibility(View.GONE);
                    }
                }
                else{
                    active = v.getId();
                    muteOthers();
                    showButtonLayout();
                }
            }
            return false;
        }
    };

    // ########## FUNCTIONS ##########

    // Sets URI to videoview and connect mediaplayer to it and starts playing video.
    public void startVideo(final int videoNumber){
        String author = "No author";
        String title = "No title";
        String desc = "No description";
        try {
            Uri videoUri = Uri.parse("rtsp://"+serverAddr+":"+rtspPort+"/"+mStreamSuffixes.get(selected));
            videos.get(videoNumber).setVideoURI(videoUri);

            // Get title and description info from JSON object
            if (mStreaminfo.getJSONObject((String) mStreamSuffixes.get(selected)).has("author"))
                author = mStreaminfo.getJSONObject((String) mStreamSuffixes.get(selected)).getString("author");
            if (mStreaminfo.getJSONObject((String) mStreamSuffixes.get(selected)).has("title"))
                title = mStreaminfo.getJSONObject((String) mStreamSuffixes.get(selected)).getString("title");
            if (mStreaminfo.getJSONObject((String) mStreamSuffixes.get(selected)).has("description"))
                desc = mStreaminfo.getJSONObject((String) mStreamSuffixes.get(selected)).getString("description");

        } catch (Exception e) {
            e.printStackTrace();
        }
        videoTexts.get(videoNumber).setText(author+" : "+title+"\n"+desc);
        infoImages.get(videoNumber).setVisibility(View.VISIBLE);
        infoImages.get(videoNumber).setImageResource(R.drawable.connecting);

        videos.get(videoNumber).requestFocus();
        videos.get(videoNumber).setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            public void onPrepared(MediaPlayer mp) {
                if(active!=videoNumber){
                    mp.setVolume(0,0);
                }
                mediaPlayers.set(videoNumber, mp);
                mediaPlayers.get(videoNumber).start();
                playbackButtons.get(videoNumber).setImageResource(R.drawable.stop_button);
                infoImages.get(videoNumber).setVisibility(View.GONE);
            }
        });
        videos.get(videoNumber).setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mp, int what, int extra) {
                infoImages.get(videoNumber).setImageResource(R.drawable.drag_and_drop);
                videoTexts.get(videoNumber).setText("");
                return false;
            }
        });
    }

    // Mutes other previews than the active one.
    // Also sets a different background color to the active layout.
    public void muteOthers(){
        for(int i=0;i<videos.size();i++){
            if (i==active){
                layouts.get(i).setBackgroundColor(getResources().getColor(R.color.active_background));
                if(videos.get(i).isPlaying()) {
                    mediaPlayers.get(i).setVolume(1, 1);
                }
            }
            else {
                layouts.get(i).setBackgroundColor(0x00000000);
                if(videos.get(i).isPlaying()) {
                    mediaPlayers.get(i).setVolume(0, 0);
                }
            }
        }
    }

    // Hides all views except the video view witch is fullscreened.
    // Sets also correct flags so that statusbar and actionbar are correclty hidden.
    public void setFullscreen(){
        if(active<0){
            return;}

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        getActionBar().hide();
        mBackLayout.setSystemUiVisibility(View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        buttonLayouts.get(active).setVisibility(View.GONE);

        ScrollView scroll = (ScrollView) findViewById(R.id.scrollView);
        scroll.setVisibility(View.GONE);

        for(int i=0;i<layouts.size();i++){
            videoTexts.get(i).setVisibility(View.GONE);
            if(i==active){
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) videos.get(i).getLayoutParams();
                params.setMargins(0,0,0,0);
                params.gravity = Gravity.CENTER;
                videos.get(i).setLayoutParams(params);

                ViewGroup.LayoutParams paramsLayout = layouts.get(i).getLayoutParams();
                paramsLayout.height = ViewGroup.LayoutParams.MATCH_PARENT;
                layouts.get(i).setLayoutParams(paramsLayout);

                continue;
            }
            videoTextScrolls.get(i).setVisibility(View.GONE);
            layouts.get(i).setVisibility(View.GONE);
            videos.get(i).setVisibility(View.GONE);
        }
    }

    // Makes all view visible again when returning from fullscreen.
    public void setPreview(){
        mBackLayout.setSystemUiVisibility(View.VISIBLE);
        getActionBar().show();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        showButtonLayout();

        ScrollView scroll = (ScrollView) findViewById(R.id.scrollView);
        scroll.setVisibility(View.VISIBLE);

        for(int i=0;i< layouts.size();i++){
            videoTexts.get(i).setVisibility(View.VISIBLE);
            if(i==active){
                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) videos.get(i).getLayoutParams();
                int margin_measures = (int) getResources().getDimension(R.dimen.videoView_margins);
                params.setMargins(margin_measures,margin_measures,margin_measures,margin_measures);
                params.gravity = Gravity.NO_GRAVITY;
                videos.get(i).setLayoutParams(params);

                LinearLayout.LayoutParams paramsLayout = (LinearLayout.LayoutParams) layouts.get(i).getLayoutParams();
                paramsLayout.height = 0;
                layouts.get(i).setLayoutParams(paramsLayout);
                continue;
            }
            videoTextScrolls.get(i).setVisibility(View.VISIBLE);
            layouts.get(i).setVisibility(LinearLayout.VISIBLE);
            videos.get(i).setVisibility(View.VISIBLE);
        }
    }

    // Creates a scrollView for thumbnails. Gets the needed info from JSON object which stores
    // all information about available streams.
    public void createScrollView(){
        GridLayout previewGrid = (GridLayout) findViewById(R.id.previewGrid);
        previewGrid.removeAllViews();

        if (mStreamSuffixes == null)
            return;

        for(int i=0;i<mStreamSuffixes.length();i++){
            try {
                ImageView image = new ImageView(this);
                TextView text = new TextView(this);

                GridLayout.Spec row = GridLayout.spec(i*2+1);
                GridLayout.Spec rowText = GridLayout.spec(i*2);
                GridLayout.Spec col = GridLayout.spec(0);
                GridLayout.LayoutParams paramsImage = new GridLayout.LayoutParams(row, col);
                GridLayout.LayoutParams paramsText = new GridLayout.LayoutParams(rowText, col);

                DisplayMetrics dm = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(dm);
                int cellWidth = dm.widthPixels /4;
                double temp = cellWidth/16;
                int cellHeight =(int) (temp*9);

                // Set correct dimensions and parameters to thumbnails and their texts.
                int margin = (int) getResources().getDimension(R.dimen.thumbnail_margins);
                paramsImage.height = cellHeight;
                paramsImage.width = ViewGroup.LayoutParams.MATCH_PARENT;
                paramsImage.setMargins(margin,0,margin,(int) getResources().getDimension(R.dimen.margin_between_thumbnails));
                image.setLayoutParams(paramsImage);

                paramsText.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                paramsText.width = ViewGroup.LayoutParams.MATCH_PARENT;
                paramsText.setMargins(margin,margin,margin,0);
                text.setLayoutParams(paramsText);

                image.setLongClickable(true);
                image.setOnLongClickListener(longListener);
                image.setOnDragListener(dragListener);
                image.setId(i);

                // Gets thumbnail pictures from server. If no url in metadata sets the default picture.
                if (mStreaminfo.getJSONObject( (String) mStreamSuffixes.get(i)).has("thumbnailUrl")) {
                    Ion.with(image)
                            .placeholder(R.drawable.stream_picture)
                            .error(R.drawable.stream_picture)
                            .load(mStreaminfo.getJSONObject((String) mStreamSuffixes.get(i)).getString("thumbnailUrl"));
                }
                else {
                    image.setImageResource(R.drawable.stream_picture);
                }

                //Set streams titles to textViews above thumbnails
                if (mStreaminfo.getJSONObject( (String) mStreamSuffixes.get(i)).has("title")){
                    String tempTextHolder = mStreaminfo.getJSONObject( (String) mStreamSuffixes.get(i)).getString("title");

                    if(tempTextHolder.length()>mMaxLengthThumbText)
                        text.setText(tempTextHolder.substring(0,mMaxLengthThumbText-1)+"...");
                    else
                        text.setText(tempTextHolder);
                }

                else
                    text.setText("No Name");

                previewGrid.addView(image);
                previewGrid.addView(text);
                previews.add(image);
                prevTexts.add(text);

            }
            catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    // Show or hides buttons. Only Buttons above active videoview are shown.
    public void showButtonLayout(){
        for(int i=0;i<buttonLayouts.size();i++){
            if(i==active)
                buttonLayouts.get(i).setVisibility(View.VISIBLE);

            else
                buttonLayouts.get(i).setVisibility(View.GONE);
        }
    }

    // Shows the dialog.
    private void showEditDialog() {
        FragmentManager fm = getSupportFragmentManager();
        EditNameDialog editNameDialog = new EditNameDialog();
        editNameDialog.show(fm, "fragment_edit_name");
        System.out.println("dialogiin");
    }


    // Creates a webSocket and creates a JSON object according the received data server.
    // Socket assumes that server sends only stream info in string (JSON format).
    // Runs in own thread.
    public void connectWebSocket() {
        URI uri;
        try {
            uri = new URI("ws://"+serverAddr+":"+webSocketPort);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            return;
        }

        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake serverHandshake) {
                Log.i("Websocket", "Opened");
                mWebSocketClient.send("Hello from " + Build.MANUFACTURER + " " + Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                String[] helper = s.split("\n");
                final String message = helper[1];
                try {
                    mStreaminfo = new JSONObject(message);
                    mStreamSuffixes = mStreaminfo.names();

                } catch (JSONException e) {
                    Log.i("Websocket", "Error in JSON parser", e);
                    e.printStackTrace();
                }
                webSocketReady = true;
            }

            @Override
            public void onClose(int i, String s, boolean b) {
                Log.i("Websocket", "Closed " + s);
            }

            @Override
            public void onError(Exception e) {
                Log.i("Websocket", "Error " + e.getMessage());
            }
        };
        mWebSocketClient.connect();
    }

    // Checks that connection to the socket is successful. Waits 5 seconds and if connection is not
    // yet established then calls function which shows dialog to the user.
    public void checkConnection() {
        connectWebSocket();
        int timeOutCounter = 0;
        while (!webSocketReady){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            timeOutCounter++;
            if (timeOutCounter>50){
                showEditDialog();
                break;
            }
        }
    }
}