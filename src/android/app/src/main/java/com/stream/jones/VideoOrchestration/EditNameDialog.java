package com.stream.jones.VideoOrchestration;


import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

public class EditNameDialog extends DialogFragment {

    public EditNameDialog() {
        // Empty constructor required for DialogFragment
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_edit_name_dialog, container);
        getDialog().setTitle("Something went wrong.");

        final Button buttonRetry = (Button) view.findViewById(R.id.buttonRetry);
        final Button buttonQuit = (Button) view.findViewById(R.id.buttonQuit);

        buttonRetry.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialog().dismiss();
                ((MainActivity) getActivity()).checkConnection();
            }
        });

        buttonQuit.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getDialog().dismiss();
                getActivity().finish();
            }
        });

        return view;
    }
}
