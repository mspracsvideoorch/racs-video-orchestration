'''
Created on Apr 22, 2015

@author: Janne Jaanila
'''

import httplib, json, sys, argparse, socket

class StreamRegistrar:
    
    def __init__(self, racs_address, racs_port, config_file_name=None):
        self.config_file_name = config_file_name
        self.racs_address = racs_address
        self.racs_port = racs_port
        self.connection = None

    def readFile(self, file_name):
        try:
            f = open(file_name, "r")
            content = f.read()
            f.close()
        except IOError:
            print "Can't open file " + file_name + "."
            exit(1)
        return content
    
    def writeFile(self, file_name, content):
        try:
            f = open(file_name, "w")
            f.write(content)
            f.close()
        except IOError:
            print "Can't open or write file " + file_name + "." 
            exit(1)
    
    def decodeJSON(self, json_content):
        content = json.loads(json_content)
        return content
    
    def encodeJSON(self, data, indent=False):
        if indent:
            content = json.dumps(data, indent=1)
        else:
            content = json.dumps(data)
        return content
    
    def sendJSONPOST(self, suffix, body):
        try:
            headers = {"Content-type": "application/json"}
            self.connection.request("POST", suffix, body=body, headers=headers)
        except (httplib.HTTPException, socket.error):
            print "sendJSONPOST connection error"
            raise
    
    def sendGET(self, suffix, body=""):
        try:
            self.connection.request("GET", suffix, body=body)
        except (httplib.HTTPException, socket.error):
            print "sendGET connection error"
            raise
    
    def requestAllStreams(self):
        try:
            self.sendGET("/get/streams")
            response = self.connection.getresponse()
        except (httplib.HTTPException, socket.error):
            print "Connection error while requesting streams."
            raise
        if (response.status == 200):
            content = self.decodeJSON(response.read())
            print self.encodeJSON(content, indent=True)
        else:
            print "Request failed due to:"
            print response.status, response.reason
            print response.read()
            exit(1)
    
    def modifyStreamMetadata(self, json_data):
        metadata_hash = {}
        metadata_hash[json_data["streamId"]] = json_data["metadata"]
        try:
            self.sendJSONPOST("/set/metadata", self.encodeJSON(metadata_hash))
            response = self.connection.getresponse()
        except (httplib.HTTPException, socket.error):
            print "Connection error while modifying stream metadata."
            raise
        if (response.status == 200):
            print response.read()
        else:
            print "Stream metadata modification failed due to:"
            print response.status, response.reason
            print response.read()
            exit(1)
    
    def addStream(self, json_data):
        address_msg = {}
        address_msg["rtspAddress"] = json_data["rtspAddress"]
        try:
            self.sendJSONPOST("/add/stream", self.encodeJSON(address_msg))
            response = self.connection.getresponse()
        except (httplib.HTTPException, socket.error):
            print "Connection error while adding stream."
            raise
        if (response.status == 200):
            print "New stream added to the server with stream ID:"
            stream_id = response.read()
            print stream_id
            json_data["streamId"] = self.decodeJSON(stream_id)["streamId"]
            self.writeFile(self.config_file_name, self.encodeJSON(json_data, indent=True))
        else:
            print "Registration failed due to:"
            print response.status, response.reason
            print response.read()
            exit(1)
        
        self.modifyStreamMetadata(json_data)
        
        print "Stream registered!"
            
    def deleteStream(self, json_data=None, stream_id=None):
        msg_content = {}
        if json_data:
            msg_content["streamId"] = json_data["streamId"]
        elif stream_id:
            msg_content["streamId"] = stream_id
        else:
            print "No json_data or stream_id defined in deleteStream."
            exit(1)
        try:
            self.sendJSONPOST("/delete/stream", self.encodeJSON(msg_content))
            response = self.connection.getresponse()
        except (httplib.HTTPException, socket.error):
            print "Connection error while deleting stream."
            raise
        if (response.status == 200):
            if json_data != None:
                del json_data["streamId"]
                self.writeFile(self.config_file_name, self.encodeJSON(json_data, indent=True))
            print response.read()
        else:
            print "Stream deletion failed due to:"
            print response.status, response.reason
            print response.read()
            exit(1)

    def run(self, action, stream_id=None):
        json_content = None
        if self.config_file_name:
            json_content = self.decodeJSON(self.readFile(self.config_file_name))#Try to decode to check the validity of the JSON config file.
        try:
            self.connection = httplib.HTTPConnection(self.racs_address, self.racs_port)
        except (httplib.HTTPException, socket.error):
            print "Can't connect to " + self.racs_address + ":" + self.racs_port + "."
            exit(1)
        if ("add" == action):
            self.addStream(json_content)
        elif ("modify" == action):
            self.modifyStreamMetadata(json_content)
        elif ("delete" == action):
            if (stream_id != None):
                self.deleteStream(json_data=json_content, stream_id=stream_id)
            elif ("streamId" in json_content):
                self.deleteStream(json_data=json_content)
            else:
                print "No streamId found in " + sys.argv[4] + "."
                print "Seems like you haven't registered that stream yet!"
                exit(1)
        elif ("requestall" == action):
            self.requestAllStreams()
        self.connection.close()
        
def parseArguments():
    parser = argparse.ArgumentParser(
        description = "Register, modify or delete a stream to RACS Stream Conductor.")
    parser.add_argument("action", choices=("add", "modify", "delete", "requestall"),
        help = "Add/modify/delete stream to RACS Stream Conductor.")
    parser.add_argument("address",
        help = "RACS Stream Conductor IP address or a domain name.")
    parser.add_argument("port", type=int, 
        help = "RACS Stream Conductor port number.")
    parser.add_argument("-i", "--config-file", required=False,
        help = "Stream's JSON config file.")
    parser.add_argument("-s", "--stream_id", required=False,
        help = "Stream ID.")
    args =  parser.parse_args()
    if args.config_file == None and (args.action == "add" or args.action == "modify"):
        parser.error("-i config_file is required with this action.")
        exit(1)
    if (args.stream_id == None and args.config_file == None) and args.action == "delete":
        parser.error("-i config_file or -s stream_id is required with this action.")
        exit(1)
    return args
    

def main():
    args = parseArguments()
    sr = StreamRegistrar(args.address, args.port, args.config_file)
    sr.run(args.action, args.stream_id)

if __name__ == '__main__':
    main()