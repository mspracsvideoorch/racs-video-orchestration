#-------------------------------------------#
Server instructions:

Boot node.js server with commands
	cd racs-video-orchestration/src/server
	node stream-conductor.js

#-------------------------------------------#
Stream registering instructions:

Register a stream to the server with command
	python streamregistrar.py add <server-address> <server-HTTP-port> -i <stream-config-file>

Delete a stream from server with command
	python streamregistrar.py delete <server-address> <server-HTTP-port> -i <stream-config-file>
or with command
	python streamregistrar.py delete -s <stream-id>

Update stream metadata with command
	python streamregistrar.py modify <server-address> <server-HTTP-port> -i <stream-config-file>

Request all stream information from the server
	python streamregistrar.py requestall <server-address> <server-HTTP-port>

You can use e.g. curl to send the same HTTP messages.

#-------------------------------------------#
Streaming instructions:

You can stream files with e.g. VLC. Use the following command
	cvlc --loop -vvv <video-filepath> --sout '#rtp{sdp=rtsp://<local-ip>:<local-port>/<suffix>}'
